# importe le module Flask
from flask import Flask
from flask import jsonify

app = Flask(__name__)

# définition d'une route GET sur l'URL racine ('/')
@app.route('/')
def hello_world():
    # envoie une réponse 'Hello World!' au client
    return 'Hello, World!'

@app.route('/data')
def data():
    return jsonify({'name': 'Pikachu', 'power': 20, 'life': 50})

